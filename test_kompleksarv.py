import unittest
from unittest import TestCase
import Praktikum3 as k
import math

class TestKompleksarv(TestCase):
    # testib kas tekstiline kuju on õige
    def test_str(self):
        self.assertEqual('1+2i', str(k.Kompleksarv(1,2)))
        self.assertEqual('1-2i', str(k.Kompleksarv(1,-2)))

    # testib kas tekib viga kui parameetrid ei ole reaalarvud
    def test_init_typeError(self):
        with self.assertRaises(TypeError):
            k.Kompleksarv("a", 1)
            k.Kompleksarv(1, "a")

    # testib kas on tõene kui Kompleksarv ei võrdu nulliga ja väär, kui võrdub nulliga
    def test_boolean(self):
        self.assertTrue(k.Kompleksarv(1, 1))
        self.assertFalse(k.Kompleksarv(1, 0))

    # testib reaalosa tagastamist
    def test_reaalosa(self):
        self.assertEqual(1, k.Kompleksarv.reaalosa(k.Kompleksarv(1,2)))
        self.assertEqual(-1, k.Kompleksarv.reaalosa(k.Kompleksarv(-1,2)))

    # testib imaginaarosa tagastamist
    def test_imaginaarosa(self):
        self.assertEqual(2, k.Kompleksarv.imaginaarosa(k.Kompleksarv(1,2)))
        self.assertEqual(-2, k.Kompleksarv.imaginaarosa(k.Kompleksarv(1,-2)))

    # testib kaaskompleksarvu tagastamist
    def test_kaaskompleksarv(self):
        self.assertEqual('1-2i', str(k.Kompleksarv.kaaskompleksarv(k.Kompleksarv(1, 2))))
        self.assertEqual('1+2i', str(k.Kompleksarv.kaaskompleksarv(k.Kompleksarv(1, -2))))

    # testib pöördarvu tagastamist
    def test_poordarv(self):
        self.assertEqual('0.2-0.4i', str(k.Kompleksarv.poordarv(k.Kompleksarv(1, 2))))
        self.assertEqual('0.2+0.4i', str(k.Kompleksarv.poordarv(k.Kompleksarv(1, -2))))

    # Kompleksarvu mooduli test
    def test_moodul(self):
        self.assertAlmostEqual(math.sqrt(5), k.Kompleksarv.moodul(k.Kompleksarv(1,2)), places=7)

    # testib hash funktsiooni
    # selleks, et hash ei muutuks on vaja muuta enviroment variable PYTHONHASHSEED=0
    def test_hash(self):
        self.assertEqual('2528504235184810679', str(hash(k.Kompleksarv(1,1))))

    # testime võrdsuse kontrolli
    def test_eq(self):
        a = k.Kompleksarv(4, 6)
        b = k.Kompleksarv(4, 6)
        c = k.Kompleksarv(8, 12)
        d = k.Kompleksarv(4, -6)
        self.assertTrue(a==b)
        self.assertFalse(a==c)
        self.assertFalse(a==d)

    # liitmine
    def test_add(self):
        self.assertEqual('2+4i', str(k.Kompleksarv(1, 2) + k.Kompleksarv(1, 2)))

    # lahutamine
    def test_sub(self):
        self.assertEqual('6+2i', str(k.Kompleksarv(9, 4) - k.Kompleksarv(3, 2)))

    # korrutamine
    def test_mul(self):
        self.assertEqual('5+12i', str(k.Kompleksarv(3, 2) * k.Kompleksarv(3, 2)))

    # Kompleksarvu vastandarv
    def test_neg(self):
        self.assertEqual('-1-2i', str(-k.Kompleksarv(1, 2)))
        self.assertEqual('1+2i', str(-k.Kompleksarv(-1, -2)))

    # jagamine
    def test_truediv(self):
        self.assertEqual('0.129411-0.082353i', str(k.Kompleksarv(1, 1) / k.Kompleksarv(2, 9)))

    # teisendamine stringist
    def test_parse_kompleksarv(self):
        self.assertEqual('1-2i', str(k.Kompleksarv.parse_kompleksarv("1-2i")))

    # teisendamine stringist ebasobiv sisend
    def test_parse_kompleksarv_input(self):
        with self.assertRaises(ValueError):
            k.Kompleksarv.parse_kompleksarv("midagi muud")
            k.Kompleksarv.parse_kompleksarv("")

    # testib polaarkuju argumendi peaväärtuse arvutamist
    def test_nurk(self):
        self.assertAlmostEqual(math.pi/12, k.Kompleksarv.nurk(k.Kompleksarv(2 + math.sqrt(3), 1)),places=6)

if __name__ == '__main__':
    unittest.main()

