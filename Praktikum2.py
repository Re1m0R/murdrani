def pistemeetod(sisend):

    #sisend=sisend.split(",")

    if len(sisend) < 2:

        return

    else:

        for i in range(1, len(sisend)):

            b = int(sisend[i])

            j = i - 1

            while j >= 0:

                if int(sisend[j]) <= b:

                    break

                sisend[j+1] = int(sisend[j])

                j -= 1

            sisend[j+1] = b

def kahendpistemeetod(sisend):

    #sisend=sisend.split(",")

    if len(sisend) < 2:

        return

    else:

        for i in range(1, len(sisend)):

            b = sisend[i]

            left = 0

            right = i

            while left < right:

                mid = int((left + right) / 2)

                if b < int(sisend[mid]):

                    right = mid

                else:

                    left = mid + 1

            sisend[left+1:i+1] = sisend[left:i]

            sisend[left] = b

def datacreator(size):

    import random

    n=0

    set=list()

    while n<size:

        num=random.randint(1,9999)

        set.append(num)

        n +=1

    return set

def apisort(sisend):

    sisend.sort()

def kiirmeetod(sisend, left, right):

    if (right - left) < 2:

        return

    x = sisend[int((left + right) / 2)]

    i = left

    j = right - 1

    while i < j:

        while sisend[i] < x:

            i += 1

        while sisend[j] > x:

            j -= 1

        if i > j:

            break

        tmp = sisend[i]

        sisend[i] = sisend[j]

        sisend[j] = tmp

        i += 1

        j -= 1

    if left < j:

        kiirmeetod(sisend, left, j + 1)

    if i < right - 1:

        kiirmeetod(sisend, i, right)

def yhildusmeetod(sisend, left, right):

    if len(sisend) < 2 or (right - left) < 2:
        return

    k = int((left + right)/2)

    yhildusmeetod(sisend, left, k)

    yhildusmeetod(sisend, k, right)

    yhilda(sisend, left, k, right)

    return sisend

def yhilda(arr, left, k, right):

    if len(arr) < 2 or (right - left) < 2 or k <= left or k >= right:
        return
    tmp = [0] * (right - left)
    n1 = left
    n2 = k
    m = 0
    while True:
        if n1 < k and n2 < right:
            if arr[n1] > arr[n2]:
                tmp[m] = arr[n2]
                n2 += 1
            else:
                tmp[m] = arr[n1]
                n1 += 1
            m += 1
        else:
            if n1 >= k:
                tmp[m:] = arr[n2:right]
            else:
                tmp[m:] = arr[n1:k]
            break
    arr[left:right] = tmp[:]

import time, statistics

korrad=int(input("Palun määrake iteratsioonide arv! "))

i=1

j=1000

pistetul=list()
kahendpistetul=list()
apitul=list()
kiirtul=list()
yhtul=list()
#radtul=list()

pistestat=list()
kahendpistestat=list()
apistat=list()
kiirstat=list()
yhstat=list()
#radstat=list()

andmemahud=list()

while j<=15000:

    andmed = datacreator(j)

    # print(andmed)

    while i<=korrad:

        start=round(time.perf_counter()*1000,2)
        pistemeetod(andmed)
        end=round(time.perf_counter()*1000,2)
        pistetul.append(end-start)

        start=round(time.perf_counter()*1000,2)
        kahendpistemeetod(andmed)
        end=round(time.perf_counter()*1000,2)
        kahendpistetul.append(end - start)

        start=round(time.perf_counter()*1000,2)
        apisort(andmed)
        end=round(time.perf_counter()*1000,2)
        apitul.append(end - start)

        start=round(time.perf_counter()*1000,2)
        kiirmeetod(andmed,0,len(andmed)-1)
        end=round(time.perf_counter()*1000,2)
        kiirtul.append(end - start)

        start = round(time.perf_counter()*1000,2)
        yhildusmeetod(andmed, 0, len(andmed) - 1)
        end = round(time.perf_counter()*1000,2)
        yhtul.append(end - start)

        '''start = round(time.perf_counter()*1000,2)
        radixsort(andmed)
        end = round(time.perf_counter()*1000,2)
        radtul.append(end - start)'''

        i +=1

    #print(statistics.mean(pistetul))
    pistestat.append(round(statistics.mean(pistetul),2))

    #print(statistics.mean(kahendpistetul))
    kahendpistestat.append(round(statistics.mean(kahendpistetul),2))

    #print(statistics.mean(apitul))
    apistat.append(round(statistics.mean(apitul),2))

    #print(statistics.mean(kiirtul))
    kiirstat.append(round(statistics.mean(kiirtul),2))

    #print(statistics.mean(yhtul))
    yhstat.append(round(statistics.mean(yhtul),2))

    #radstat.append(round(statistics.mean(radtul), 2))

    andmemahud.append(j)

    j +=1000

    i=1

    pistetul.clear()
    kahendpistetul.clear()
    apitul.clear()
    kiirtul.clear()
    yhtul.clear()
    #radtul.clear()

print("Iga meetodit testiti "+str(korrad)+" korda. " + "\n")
print("Andmemahud," + "\t" + str(andmemahud))
print("Pistemeetodi keskmised," + "\t" + str(pistestat))
print("Kahendpistemeetodi keskmised," + "\t" + str(kahendpistestat))
print("API keskmised," + "\t" + str(apistat))
print("Kiirmeetodi keskmised," + "\t" + str(kiirstat))
print("Ühildamismeetodi keskmised," + "\t" + str(yhstat))
#print("RADIX meetodi keskmised," + "\t" + str(yhstat))

