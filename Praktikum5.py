class Node(object):

    __name = 'noname_node'
    __first_child = None
    __next_sibling = None

    def __init__(self, name):

        self.__name = name

    def get_name(self):

        return self.__name

    def set_name(self, name):

        self.__name = name

    def get_first_child(self):

        return self.__first_child

    def set_first_child(self, node):

        self.__first_child = node

    def get_next_sibling(self):

        return self.__next_sibling

    def set_next_sibling(self, node):

        self.__next_sibling = node

    def add_child(self, tipp):

        if tipp:

            if not self.get_first_child():

                self.set_first_child(tipp)

            else:

                laps = self.get_first_child()

                while laps.get_next_sibling():

                    laps = laps.get_next_sibling()

                laps.set_next_sibling(tipp)

    @staticmethod
    def TekitaPuu(tippe,lapsi,nimi):

        import random

        pikkus=random.randint(1,int(tippe))

        valik=[]

        i=pikkus

        tulemus="Tippude nimed on: \n"

        while i>=1:

            if i==pikkus:

                tipp=Node(nimi)

                tulemus=tulemus+str(nimi)+"\n"

                valik.append(tipp)

                i -= 1

            maxvanemaid=len(valik)

            jrk=random.randint(0,maxvanemaid-1)

            vanemanimi=valik[jrk].get_name()

            if i+1==pikkus:

                vanem=tipp

            else:

                vanem=valik[jrk]

            valik.remove(vanem)

            jooksevlaius = min(i,random.randint(1, int(lapsi)))

            i-=jooksevlaius

            j=1

            while j<=jooksevlaius:

                uusnimi=str(vanemanimi)+"."+str(j)

                tulemus=tulemus+uusnimi+"\t"

                laps=Node(uusnimi)

                vanem.add_child(laps)

                valik.append(laps)

                j+=1

            tulemus=tulemus+"\n"

        print("Tippe peaks olema "+str(pikkus)+". ")

        print("\n"+str(tulemus))

        return tipp

    def VasakSulg(self,list,it=1):

        tul=list

        if it==1: tul.clear

        tul.append(self.get_name())

        child =self.get_first_child()

        if child: tul.append("(")

        while child:

            tul=child.VasakSulg(tul,2)

            if child.get_next_sibling():

                tul.append(",")

                child = child.get_next_sibling()

            else:

                tul.append(")")

                child = child.get_next_sibling()

        if it==1:

            tulemus = ""

            for element in tul:
                tulemus = tulemus + str(element)

            tul.clear
            list.clear

            return tulemus

        if it==2: return tul

    def main(arv):

        import random

        n=1

        while n<=arv:

            tip=random.randint(1,20)

            lap=random.randint(1,5)

            print(str(n)+". Lubati genereerida "+ str(tip) +" tippu ja "+str(lap)+" last. \n")

            puu=Node.TekitaPuu(tip, lap, n)

            print("Vasakpoolne suluesitus on: \n"+puu.VasakSulg([])+"\n")

            n+=1

Node.main(10)

'''juur=Node("A")
juur.add_child(Node("B"))
juur.add_child(Node("C"))
juur.get_first_child().add_child(Node("D"))
juur.get_first_child().get_first_child().add_child(Node("G"))
juur.get_first_child().get_first_child().add_child(Node("H"))
juur.get_first_child().add_child(Node("E"))
juur.get_first_child().add_child(Node("F"))
juur.get_first_child().get_first_child().get_next_sibling().get_next_sibling().add_child(Node("I"))
juur.get_first_child().get_next_sibling().add_child(Node("J"))
print(juur.VasakSulg([]))'''