class Kompleksarv(object):

    # Praktikum 3

    __reaalosa=0

    __imaginaarosa=0

    def reaalosa(self):

        return self.__reaalosa

    def imaginaarosa(self):

        return self.__imaginaarosa

    def kaaskompleksarv(self):

        uus=self.__imaginaarosa*-1

        vastus=Kompleksarv(self.__reaalosa,uus)

        return vastus

    def __neg__(self):

        a=self.__reaalosa*-1
        b=self.__imaginaarosa*-1

        vastus=Kompleksarv(a,b)

        return vastus

    def omistavaartus(self,reaal,imaginaar):

        self.__reaalosa = reaal

        self.__imaginaarosa = imaginaar

    def __init__(self, re, im):

        if type(re) is str or type(im) is str:

            raise TypeError("Palun sisestage numbrid!")

        #if re==0 and im==0 or re==0:

            #raise ValueError("Mõlemad väärtused 0!")

        self.omistavaartus(re,im)

    def __str__(self):

        if self.__imaginaarosa < 0:

            mark=""

        else:

            mark="+"

        return str(self.__reaalosa) + mark + str(self.__imaginaarosa) + "i"

    def __hash__(self):

        return hash((round(self.__reaalosa,10), round(self.__imaginaarosa),10))

    def __eq__(self, teine):

        a=round(self.__reaalosa,10)
        b=round(teine.__reaalosa,10)
        c=round(self.__imaginaarosa,10)
        d=round(teine.__imaginaarosa,10)

        return a == b and c == d

    def __bool__(self):

        return self.__reaalosa != 0 and self.__imaginaarosa != 0

    def __add__(self,teine):

        esimenetykk=round(self.__reaalosa,6) + round(teine.__reaalosa,6)
        teinetykk=round(self.__imaginaarosa,6) + round(teine.__imaginaarosa,6)

        vastus=Kompleksarv(esimenetykk,teinetykk)

        return vastus

    def __mul__(self, teine):

        esimenetykk=round(self.__reaalosa*teine.__reaalosa-self.__imaginaarosa*teine.__imaginaarosa,6)
        teinetykk=round(self.__reaalosa*teine.__imaginaarosa+self.__imaginaarosa*teine.__reaalosa,6)

        vastus=Kompleksarv(esimenetykk,teinetykk)

        return vastus

    def poordarv(self):

        '''if bool(self)!=True:
            raise ValueError("Sisendiks 0")'''

        esimenetykk=round(self.__reaalosa/(self.__reaalosa*self.__reaalosa+self.__imaginaarosa*self.__imaginaarosa),6)
        teinetykk=round(-self.__imaginaarosa/(self.__reaalosa*self.__reaalosa+self.__imaginaarosa*self.__imaginaarosa),6)

        vastus=Kompleksarv(esimenetykk,teinetykk)

        return vastus

    def __sub__(self,teine):

        esimenetykk = round(self.__reaalosa, 6) - round(teine.__reaalosa, 6)
        teinetykk = round(self.__imaginaarosa, 6) - round(teine.__imaginaarosa, 6)

        vastus=Kompleksarv(esimenetykk,teinetykk)

        return vastus

    def __truediv__(self, teine):

        if teine ==Kompleksarv(0,0): raise ValueError("Nulliga jagamine")

        teine2=teine.poordarv()

        vastus=self*teine2


        return vastus

    def moodul(self):

        import math

        a=self.__reaalosa
        b=self.__imaginaarosa

        vastus=math.sqrt(a*a+b*b)

        return vastus

    def nurk(self):

        import math

        x=self.__reaalosa
        y=self.__imaginaarosa
        if self==Kompleksarv(0,0): raise ValueError("Nullist ei saa nurka leida")

        return math.atan2(y, x)
        '''r=self.moodul()

        jagatis=y/x

        tulemus=math.atan(jagatis)

        o=math.degrees(tulemus)

        esimenetykk=r*math.cos(math.radians(o))

        teinetykk=r*math.sin(math.radians(o))

        vastus=Kompleksarv(esimenetykk,teinetykk)

        return tulemus'''

    @staticmethod
    def parse_kompleksarv(tekst):

        mark=""

        if tekst[0]=="-":

            tekst = tekst.lstrip("-")

            mark="-"

        if "+" in tekst:

            tulemus=tekst.split("+")

            tulemus[1] = eval(("+" + tulemus[1].strip("i")) * 1)

        elif "-" in tekst:
            tulemus=tekst.split("-")

            #print(tulemus[1])

            tulemus[1]=eval(("-"+tulemus[1].strip("i"))*1)

        #print(valik)

        if mark=="-":
            tulemus[0]=eval(("-"+tulemus[0])*1)

        else:
            tulemus[0] = eval(("+" + tulemus[0])*1)

        a=tulemus[0]
        b=tulemus[1]

        #print(str(a) +"and" + str(b))

        vastus=Kompleksarv(a,b)

        return vastus

'''minuarv=Kompleksarv(4,6)

temaarv=Kompleksarv(2,3)

print(minuarv)

print(temaarv)

print(hash(minuarv))

print(hash(temaarv))

print(minuarv.kaaskompleksarv())

print(minuarv==temaarv)

print(bool(minuarv))

print(-minuarv)

print(minuarv+temaarv)

print(minuarv*temaarv)

print(minuarv.poordarv())

print(minuarv-temaarv)

print(minuarv/temaarv)

print(minuarv.moodul())

print(minuarv.nurk())

print(minuarv.__str__())

print(Kompleksarv.parse_kompleksarv("1-2i"))

minuarv=Kompleksarv(1,1)

print(hash(minuarv))

temaarv=Kompleksarv(2,9)

print(minuarv/temaarv)'''

for i in range(0, 360):

    import math
    c1 = Kompleksarv(math.cos(math.radians(i)), math.sin(math.radians(i)))
    m = c1.moodul()
    if m==0: raise ValueError
    p = c1.nurk()

    if c1!=Kompleksarv(m * math.cos(p), m * math.sin(p)):
        print(i)

        print(c1)

        #print(str(m) + " and " + str(p))
        print(Kompleksarv(m * math.cos(p), m * math.sin(p)))
        print("\n")





'''for i in range(-1, 2):

    #print(i)
    for j in range(-1, 2):
        #print(j)
        c1 = Kompleksarv(i, j)
        s1 = c1.__str__()
        #print(s1)

        #print(c1)
        c2 = Kompleksarv.parse_kompleksarv(s1)


        print(c2)

        #print(c1==c2)'''