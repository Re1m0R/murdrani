class BIT():

    __name = None
    __cumulvalue = None
    __first_child = None
    __next_sibling = None

    def __init__(self, name,cumulvalue):

        self.__name = name

        self.__cumulvalue=cumulvalue

    def get_name(self):

        return self.__name

    def set_name(self, name):

        self.__name = name

    def get_first_child(self):

        return self.__first_child

    def set_first_child(self, node):

        self.__first_child = node

    def get_next_sibling(self):

        return self.__next_sibling

    def set_next_sibling(self, node):

        self.__next_sibling = node

    def add_child(self, tipp):

        if tipp:

            if not self.get_first_child():

                self.set_first_child(tipp)

            else:

                laps = self.get_first_child()

                while laps.get_next_sibling():

                    laps = laps.get_next_sibling()

                laps.set_next_sibling(tipp)

    @staticmethod
    def ConvertArray(array):

        for x in range(1,len(array)+1):

            array[x-1]=int(array[x-1])

        # Toetub paremas suunas bitwise liikumisele

        for index in range(1,len(array)+1):

            #print("see on esialgne indeks: " + str(bin(idx)))

            bitwiseindex = index + (index & -index)

            #print("see on järgmine bitwise indeks: " + str(bin(bitwiseindex)))

            if bitwiseindex <= len(array):

                array[bitwiseindex-1] = array[bitwiseindex-1]+array[index-1]

        return array

    def ArraySum(array, index):

        # Toetub vasakus suunas bitwise liikumisele

        summa = 0

        while index:

            summa += array[index-1]

            index -= index & -index

        return summa

    def ArrayUpdate(array, index, value):

        # Toetub taaskod paremas suunas bitwise liikumisele

        while index <= len(array):

            array[index-1] += value

            index += index & -index

        return array

    def ArrayTree(array):

        tipud=[]

        for n in range(1,len(array)+1):

            tipud.append(BIT(n,array[n-1]))

        for node in range(1,len(tipud)+1):

            parentnode = node + (node & -node)

            if parentnode <= len(tipud):

                tipud[parentnode-1].add_child(tipud[node-1])

        count=BIT.TreeCount(array)

        n=0

        while n<len(count):

            tree=tipud[count[n]]

            print(tree.leftParenthesis([]))

            n+=1

    def leftParenthesis(self,list,it=1):

        tul=list

        if it==1: tul.clear

        tul.append(self.get_name())

        child =self.get_first_child()

        if child: tul.append("(")

        while child:

            tul=child.leftParenthesis(tul,2)

            if child.get_next_sibling():

                tul.append(",")

                child = child.get_next_sibling()

            else:

                tul.append(")")

                child = child.get_next_sibling()

        if it==1:

            tulemus = ""

            for element in tul:
                tulemus = tulemus + str(element)

            tul.clear
            list.clear

            return tulemus

        if it==2: return tul

    def TreeCount(array):

        toodeldud = len(array)

        juured = []

        i = 0

        while toodeldud > 0:

            if i < len(array):

                maksimum = max(array[i:len(array)])

            else:
                break

            jrk = array[i:len(array)].index(maksimum)+i

            juured.append(jrk)

            i = jrk + 1

            toodeldud =len(array)-i

        return juured

    def main():

        jada=input("Palun sisestage jada (komaga eraldatud)! \n")

        jada=jada.split(",")

        print("\nSisestasite jada "+str(jada)+". \n")

        bit = BIT.ConvertArray(jada)

        print("Kahendindekseeritudpuu massiivina oleks " + str(bit) + ". \n")

        summa=int(input("Mitme liikme summat soovite? "))

        print("\nEsimese "+str(summa) + " liikme summa on "+str(BIT.ArraySum(bit,summa))+". \n")

        uuendus=input("Palun sisestage uuendatava liikme positsioon ja kui palju soovite vähendada/suurendada (komaga eraldatud)! \n")

        uuendus=uuendus.split(",")

        print("\nUuendatud kahendindekseeritudpuu massiivina tuleb " + str(BIT.ArrayUpdate(bit,int(uuendus[0]), int(uuendus[1]))) + ". \n")

        print("Selle massiivi puu(d) oleks: ")

        BIT.ArrayTree(bit)

if __name__ == '__main__':
    BIT.main()